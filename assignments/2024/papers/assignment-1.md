# Assignment

- Course Title: **Trends in Artificial Intelligence and Machine Learning**
- Course Code: **TAI911S**
- Assessment: First Assignment
- Released on: 25/03/2024
- Due Date: 09/04/2024

# Problem

## Problem Description

The table below contains URLs of economic datasets (__Ecdat__) compiled in the **RDatasets** package. Given the group number assigned to you, select the corresponding dataset and perform the following tasks:

- complete an exploratory data **analysis**;
- select the most  appropriate **regression function**. You will motivate your selection;
- build a model based on the selected regression function and test it.

All implementations should be completed in the **Julia** Programming language.

The list of datasets is as follows. 


|Number|Dataset Name|Target Variable|
|-------------|---------|-----------|
|1|BudgetFood|wfood|
|2|Computers|price|
|3|Earnings|ahe|
|4|Fishing|price|
|5|Heating|rooms|
|6|MunExp|expend|
|7|PatentsRD|patent|
|8|PSID|earnings|
|9|Star|schidkn|
|10|SumHes|sr|
|11|Tobacco|lnx|

Note that you can access these datasets using various packages, such as the combination of **RDatasets** and **Tidier**.

## Assessment Criteria

We will follow the criteria below to assess the problem:

- Exploratory data analysis. (25%)
- Motivation for the regression function (15%)
- Model implementation. (40%)
- Evaluation metrics and discussions. (15%)
- Overall quality of solution in **Julia**. (5%)

# Submission Instructions

- This assignment is to be completed by groups of students as already set.
- For each group, a repository should be created on [Gitlab](https://about.gitlab.com).
- The submission date is Tuesday, April 9 2024, at midnight. Please note that _commits_ after that deadline will not be accepted. Therefore, a submission will be assessed based on the repository's clone at the deadline.
- There will be a mark deduction for each day of delay.
- Each group is expected to present its project after the submission deadline. Each student within a group will have to demonstrate their contribution.
- In the case of plagiarism (groups copying from each other or submissions copied from the Internet), all submissions involved will be awarded the mark 0, and each student will receive a warning.
